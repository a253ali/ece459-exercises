// You should implement the following function:

fn sum_of_multiples(number: i32, multiple1: i32, multiple2: i32) -> i32 {
    let sum1 = arith_series(number, multiple1);
    let sum2 = arith_series(number, multiple2);
    let repeated = repeated_series(number, multiple1, multiple2);

    return sum1 + sum2 - repeated;
}

fn repeated_series(number: i32, multiple1: i32, multiple2: i32) -> i32 {
    let lcm = multiple1 * multiple2;
    return arith_series(number, lcm);
}

fn arith_series(number: i32, multiple: i32) -> i32 {
    let mut upper_bound = number - 1;
    while upper_bound % multiple != 0 {
        upper_bound -= 1;
    }
    let num_terms = upper_bound / multiple;

    return num_terms * (multiple + upper_bound) / 2;
}

fn main() {
    println!("Sum of Multiples: {}", sum_of_multiples(1000, 5, 3));
}
